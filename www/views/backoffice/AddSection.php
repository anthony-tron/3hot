    <main class="container">
        <form action="" method="post">
            <div class="mb-3">
                <label for="inputSectionTitle" class="form-label">Titre de la section</label>
                <input name="name" type="text" class="form-control" id="inputSectionTitle" placeholder="Titre de la section">
            </div>
            <button type="submit" class="btn btn-primary m-auto d-block">Ajouter la section</button>
        </form>
    </main>