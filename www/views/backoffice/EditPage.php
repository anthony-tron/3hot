    <main class="container">
        <form method="post" action="">
            <div class="mb-3">
                <label for="inputPageTitle" class="form-label">Titre de la page</label>
                <input name="title" type="text" class="form-control" id="inputPageTitle" placeholder="Titre de la page"
                    value="<?= $page->getTitle() ?>"
                >
            </div>
            <div class="mb-3">
                <label for="inputPageContent" class="form-label">Contenu de la page</label>
                <textarea name="content" class="form-control" id="inputPageContent" rows="10" placeholder="Contenu de la page"><?= $page->getContent() ?></textarea>
            </div>
            <div class="mb-3">
                <label for="selectSection" class="form-label">Rattacher à la section</label>
                <select name="section" class="form-select" id="selectSection" aria-label="Select liste sections">
<?php foreach($sections as $section): ?>
                        <option
                            value="<?= $section->getID() ?>"
    <?php if($section->getID() === $page->getSection()->getID()): ?>
                            selected="selected"
    <?php endif; ?>
                        >
                            <?= $section->getName() ?>
                        </option>
<?php endforeach; ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary m-auto d-block">Modifier la page</button>
        </form>
    </main>