    <main class="container">
        <form method="post" action="">
            <div class="mb-3">
                <label for="inputSectionTitle" class="form-label">Titre de la section</label>
                <input
                    name="name"
                    type="text"
                    class="form-control"
                    id="inputSectionTitle"
                    placeholder="Titre de la section"
                    value="<?= $section->getName() ?>"
                />
            </div>
            <button type="submit" class="btn btn-primary m-auto d-block">Modifier la section</button>
        </form>
    </main>