    <main class="container">
        <section class="row">
            <article class="col-12 col-md-8 mb-5">
                <h2>Pages (documentation)</h2>
                <a href="admin/page/add" role="button" class="btn btn-outline-primary mb-3">+ Ajouter</a>
                <form method="post" action="/admin/page/delete">
                    <table class="table table-bo table-hover">
                        <thead class="table-light">
                            <tr>
                                <th scope="col" style="width: 30px;">
                                    <input type="checkbox" id="selectAllPages" class="form-check-input" value="">
                                    <label for="selectAll" class="d-none"></label>
                                </th>
                                <th scope="col">id</th>
                                <th scope="col">nom</th>
                                <th scope="col">section associée</th>
                                <th scope="col">actions</th>
                            </tr>
                        </thead>
                        <tbody class="tbody-pages">
<?php foreach($pages as $page): ?>
                            <tr>
                                <td>
                                    <label>
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            name="ids[]"
                                            value="<?= $page->getID() ?>"
                                        />
                                    </label>
                                </td>
                                <th scope="row">
                                    <?= $page->getID() ?>
                                </th>
                                <td>
                                    <a href="documentation/page/<?= $page->getID() ?>" target="_blank" title="Voir la page">
                                        <?= $page->getTitle() ?>
                                    </a>
                                    <i class="bi bi-box-arrow-up-right external-link-bo"></i>
                                </td>
                                <td>
                                    <i>
                                        <?= $page->getSection()->getName() ?>
                                    </i>
                                </td>
                                <td>
                                    <a role="button" class="btn btn-sm btn-light btn-action" href="admin/page/edit/<?= $page->getID() ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier"><i class="bi bi-pencil-fill"></i></a>
                                </td>
                            </tr>
<?php endforeach; ?>

                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-secondary btn-delete-selection-pages" disabled>Supprimer les éléments</button>
                </form>
            </article>

            <article class="col-12 col-md-4 mb-5">
                <h2>Sections</h2>
                <a href="admin/section/add" role="button" class="btn btn-outline-primary mb-3">+ Ajouter</a>

                <form action="/admin/section/delete" method="post">
                    <table class="table table-bo table-hover">
                        <thead class="table-light">
                        <tr>
                            <th scope="col" style="width: 30px;">
                                <input type="checkbox" id="selectAllSections" class="form-check-input" value="">
                                <label for="selectAll" class="d-none"></label>
                            </th>
                            <th scope="col">id</th>
                            <th scope="col">nom</th>
                            <th scope="col">actions</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-sections">
<?php foreach ($sections as $section): ?>
                            <tr>
                                <td>
                                    <label>
                                        <input
                                                name="ids[]"
                                                value="<?= $section->getID() ?>"
                                                class="form-check-input"
                                                type="checkbox"
                                        />
                                    </label>
                                </td>
                                <th scope="row">
                                    <?= $section->getID() ?>
                                </th>
                                <td>
                                    <a
                                            href="documentation/section/<?= $section->getID() ?>"
                                            target="_blank"
                                            title="Voir la section"
                                    >
                                        <?= $section->getName() ?>
                                    </a>
                                    <i class="bi bi-box-arrow-up-right external-link-bo"></i>
                                </td>
                                <td>
                                    <a
                                            role="button"
                                            class="btn btn-sm btn-light btn-action"
                                            href="admin/section/edit/<?= $section->getID() ?>"
                                            data-bs-toggle="tooltip"
                                            data-bs-placement="top" title="Modifier">
                                        <i class="bi bi-pencil-fill"></i>
                                    </a>
                                </td>
                            </tr>
<?php endforeach; ?>
                        </tbody>
                    </table>

                    <button type="submit" class="btn btn-secondary btn-delete-selection-sections" disabled>Supprimer les éléments</button>
                </form>
            </article>
            <article class="col-12 col-md-8 mb-5">
                <h2>Utilisateurs</h2>
                <a href="admin/user/add" role="button" class="btn btn-outline-primary mb-3">+ Ajouter</a>


                <form action="/admin/user/delete" method="post">

                    <table class="table table-bo table-hover">
                        <thead class="table-light">
                        <tr>
                            <th scope="col" style="width: 30px;">
                                <input type="checkbox" id="selectAllUsers" class="form-check-input" value="">
                                <label for="selectAll" class="d-none"></label>
                            </th>
                            <th scope="col">id</th>
                            <th scope="col">nom d'utilisateur</th>
                            <th scope="col">email</th>
                            <th scope="col">rôle</th>
                            <th scope="col">actions</th>
                        </tr>
                        </thead>
                        <tbody class="tbody-users">
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td>
                                    <label>
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            name="ids[]"
                                            value="<?= $user->getID() ?>"
                                        />
                                    </label>
                                </td>
                                <th scope="row">
                                    <?= $user->getID() ?>
                                </th>
                                <td>
                                    <?= $user->getName() ?>
                                </td>
                                <td>
                                    <?= $user->getEmail() ?>
                                </td>
                                <td>
                                    <?= $user->getRole()->getName() ?>
                                </td>
                                <td>
                                    <a
                                            href="admin/user/edit/<?= $user->getID() ?>"
                                            role="button"
                                            class="btn btn-sm btn-light btn-action"
                                            data-bs-toggle="tooltip"
                                            data-bs-placement="top"
                                            title="Modifier"><i class="bi bi-pencil-fill"></i>
                                    </a>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <button
                        type="submit"
                        class="btn btn-secondary btn-delete-selection-users"
                        disabled
                    >
                        Supprimer les utilisateurs sélectionnés
                    </button>

                </form>


            </article>
        </section>

    </main>

<!-- TODO fix le chemin -->
<script src="../../assets/js/backoffice/manageTableCheckboxes.js" type="text/javascript"></script>