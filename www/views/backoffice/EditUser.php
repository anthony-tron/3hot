    <main class="container">
        <form method="post" action="">
            <p>
                <label for="inputUsername" class="form-label">Nom d'utilisateur</label>
                <input type="text" class="form-control" id="inputUsername" name="name" value="<?= $user->getName() ?>">
            </p>
            <p>
                <label for="inputEmail" class="form-label">Adresse mail</label>
                <input type="email" class="form-control" id="inputEmail" name="email" value="<?= $user->getEmail() ?>">
            </p>
            <button type="submit" class="btn btn-primary">Modifier l'utilisateur</button>
        </form>
    </main>