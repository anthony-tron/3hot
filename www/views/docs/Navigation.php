<nav class="list-group menu-documentation col-12 col-lg-2 pe-0 pe-lg-3 mb-3">
    <?php foreach ($sections as $section): ?>
        <a
            href="/documentation/section/<?= $section->getID() ?>"
            class="list-group-item list-group-item-action"
        >
            <?= $section->getName() ?>
        </a>
    <?php endforeach; ?>
</nav>
