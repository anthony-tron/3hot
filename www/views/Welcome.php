<main class="container-fluid container-accueil">
    <!-- Section températures -->
    <article class="article-front row g-0">
        <h2 class="h2-front">Récap journalier</h2>
        <!-- Température intérieure -->
        <section class="col-sm-6 text-center">
            <h3>Intérieur</h3>
            <strong class="main-temp" id="int-current">…</strong>
            <a role="button" tabindex="0" class="color-primary position-absolute ms-2" id="alerte-int" data-bs-toggle="popover">
                <i class="bi bi-info-circle-fill"></i>
            </a>
            <ul>
                <li class="d-inline-flex flex-column p-1">Min.
                    <ul>
                        <li id="int-min">…</li>
                    </ul>
                </li>
                <li class="d-inline-flex flex-column p-1">Max.
                    <ul>
                        <li id="int-max">…</li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- Température extérieure -->
        <section class="col-sm-6 text-center">
            <h3>Extérieur</h3>
            <strong class="main-temp" id="ext-current">…</strong>
            <a role="button" tabindex="0" class="color-primary position-absolute ms-2" id="alerte-ext">
                <i class="bi bi-info-circle-fill"></i>
            </a>
            <ul>
                <li class="d-inline-flex flex-column p-1">Min.
                    <ul>
                        <li id="ext-min">…</li>
                    </ul>
                </li>
                <li class="d-inline-flex flex-column p-1">Max.
                    <ul>
                        <li id="ext-max">…</li>
                    </ul>
                </li>
            </ul>
        </section>
    </article>
    <!-- Section historique -->
    <article class="article-front">
        <h2 class="h2-front">Synthèse hebdomadaire</h2>
        <!-- Graphique hebdomadaire -->
        <section class="mb-4">
            <h3>Moyenne</h3>
            <div style="height:280px;">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </section>
        <!-- Températures maximales et minimales -->
        <section>
            <h3>Minimales et maximales</h3>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#onglet-interieur" type="button" role="tab" aria-controls="onglet-interieur" aria-selected="true">Intérieur</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#onglet-exterieur" type="button" role="tab" aria-controls="onglet-exterieur" aria-selected="false">Extérieur</button>
                </li>
            </ul>
            <!-- Tabber -->
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="onglet-interieur" role="tabpanel" aria-labelledby="onglet-interieur">
                    <div class="table-responsive">
                        <table class="table table-bordered text-center mb-0">
                            <thead>
                            <tr class="table-light">
                                <th colspan="2">Lundi</th>
                                <th colspan="2">Mardi</th>
                                <th colspan="2">Mercredi</th>
                                <th colspan="2">Jeudi</th>
                                <th colspan="2">Vendredi</th>
                                <th colspan="2">Samedi</th>
                                <th colspan="2">Dimanche</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>20.9°C</td>
                                <td>21.7°C</td>
                                <td>20.6°C</td>
                                <td>22.1°C</td>
                                <td>20.8°C</td>
                                <td>21.5°C</td>
                                <td>19.9°C</td>
                                <td>21.4°C</td>
                                <td>20.3°C</td>
                                <td>21.1°C</td>
                                <td>20.6°C</td>
                                <td>21.5°C</td>
                                <td>20.9°C</td>
                                <td>21.7°C</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="onglet-exterieur" role="tabpanel" aria-labelledby="onglet-exterieur">
                    <div class="table-responsive">
                        <table class="table table-bordered text-center mb-0">
                            <thead>
                            <tr class="table-light">
                                <th colspan="2">Lundi</th>
                                <th colspan="2">Mardi</th>
                                <th colspan="2">Mercredi</th>
                                <th colspan="2">Jeudi</th>
                                <th colspan="2">Vendredi</th>
                                <th colspan="2">Samedi</th>
                                <th colspan="2">Dimanche</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>14.1°C</td>
                                <td>22.6°C</td>
                                <td>15.4°C</td>
                                <td>18.3°C</td>
                                <td>14.2°C</td>
                                <td>18.7°C</td>
                                <td>11.9°C</td>
                                <td>17.4°C</td>
                                <td>12.3°C</td>
                                <td>18.8°C</td>
                                <td>8.2°C</td>
                                <td>16.4°C</td>
                                <td>7.9°C</td>
                                <td>15.4°C</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </article>
</main>