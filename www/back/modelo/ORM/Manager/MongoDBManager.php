<?php

namespace modelo\ORM\Manager;

class MongoDBManager implements DBManager
{

    private $mongo;

    public function __construct(string $host)
    {
        $this->mongo = new \MongoDB($host);
    }

    /**
     * @param String $table
     * @param array $values
     * @return mixed
     */
    public function create(string $table, array $values)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param String $table
     * @param array $values
     * @param array $criteres
     * @return mixed
     */
    public function read(string $table, array $values, array $criteres)
    {
        // TODO: Implement read() method.
    }

    /**
     * @param String $table
     * @param array $values
     * @param array $criteres
     * @return mixed
     */
    public function update(string $table, array $values, array $criteres)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param String $table
     * @param array $criteres
     * @return mixed
     */
    public function delete(string $table, array $criteres)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return mixed
     */
    public function getTables()
    {
        // TODO: Implement getTables() method.
    }

    /**
     * @param String $table
     * @param array $column
     * @return mixed
     */
    public function newTable(string $table, array $column)
    {
        // TODO: Implement newTable() method.
    }

    /**
     * @param String $table
     * @param array $column
     * @return mixed
     */
    public function updateTable(string $table, array $column)
    {
        // TODO: Implement updateTable() method.
    }
}