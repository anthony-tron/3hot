<?php

namespace modelo\Repository;

use modelo\ORM\Collection;
use modelo\ORM\DBConnector;
use modelo\ORM\ORM;
use modelo\Table\AbstractTable;

abstract class AbstractRepository
{
    private $classSetterMethodes;

    private $classGetterMethodes;

    private $table;

    private $tableName;

    private $orm;

    public function __construct($table)
    {
        $this->orm = ORM::getORM();

        $this->table = $table;

        $className = explode("\\", $table);

        $this->tableName = $className[sizeof($className) -1];

        $this->classSetterMethodes = new \ReflectionClass($table);

        $this->classSetterMethodes = $this->classSetterMethodes->getMethods();

        $methodeListe = [];
        foreach ($this->classSetterMethodes as $methode)
        {
            if(substr($methode->getName(), 0, 3) === 'set')
            {
                $methodeListe[] = $methode->getName();
            }

            if (substr($methode->name, 0, 3) === 'get')
            {
                $this->classGetterMethodes[] = $methode->getName();
            }
        }
        $this->classSetterMethodes = $methodeListe;
    }

    /**
     * Fabrique un objet de type Table à partir d'un enregistrement en DB
     * @param array $fields Tableau des champs d'un enregistrement en DB
     * @return AbstractTable|null
     * @throws \ReflectionException
     */
    protected function build(array $fieldsFromBase, bool $isManyToMany = false, array $ignoreFkFields = []) : ?AbstractTable
    {
        if (!empty($fieldsFromBase)) {
            $tableClass = new $this->table();
            $attrsTableClass = new \ReflectionClass($this->table);
            $parent = $attrsTableClass->getParentClass();
            $id = new \ReflectionClass($parent->getName());
            $id = $id->getProperties()[1];
            $attrsTableClass = $attrsTableClass->getProperties();
            $attrsTableClass[] = $id;

            /**
             * Gestion de relation ManyToMany
             * TODO: Améliorer le test $isManyToMany => permet d'empêcher une recherche circulaire en base
             */
            $fkManyToMany = [];

            if (!$isManyToMany) {
                $dbTables = $this->orm->importDATABASE();
                foreach ($dbTables as $dbTableName => $dbTableColumn) {
                    $parseTableNameManyToMany = explode('_', $dbTableName);
                    if ((sizeof($parseTableNameManyToMany) > 1) && in_array(strtolower($this->tableName), $parseTableNameManyToMany)) {
                        foreach ($parseTableNameManyToMany as $tableManyToMany) {
                            if ($tableManyToMany != strtolower($this->tableName)) {
                                $targetID = $this->makeRequest("READ fk_" . $tableManyToMany . " WHERE fk_" . strtolower($this->tableName) . " = " . $fieldsFromBase[0]['id'], $dbTableName, true);
                                foreach ($targetID as $value) {
                                    $fkManyToMany[$tableManyToMany][] += $value["fk_" . $tableManyToMany];
                                } //foreach
                            } //if
                        } //foreach
                    } //if
                } //foreach
            }

            /*
             * Gestion de la relation ManyToOne
             */
            $tables = $this->orm->importDATABASE();
            foreach ($tables as $tableNameFromBase => $tableFieldsFromBase) {
                foreach($tableFieldsFromBase as $tableFieldFromBase) {
                    $parseTableName = explode('fk_', $tableFieldFromBase['Field']);
                    if (isset($parseTableName[1]) && $parseTableName[1] == strtolower($this->tableName)) {
                        $repoTarget = 'app\Repository\\' . ucfirst($tableNameFromBase) . 'Repository';
                        $repoTarget = new $repoTarget();
                        $object = $repoTarget->findBy([$tableFieldFromBase['Field'] => $fieldsFromBase['id']], [$tableFieldFromBase['Field']]);
                        if (!is_null($object)) $manyToOneValues[$tableNameFromBase] = $object;
                    }
                }
            }

            foreach ($attrsTableClass as $attrTableClass) {
                foreach ($fieldsFromBase as $attrNameFromBase => $valueFieldFromBase) {
                    /**
                     * Gestion clé étrangères
                     */
                    $fk = null;

                    if (explode("_", $attrNameFromBase)[0] == 'fk' && !in_array($attrNameFromBase, $ignoreFkFields)) {
                        $parseAttrNameFromBase = explode("_", $attrNameFromBase);
                        if ($parseAttrNameFromBase[1] == $attrTableClass->getName()) {
                            $fk = $parseAttrNameFromBase[1];
                        } //if
                    } //if

                    if (($attrNameFromBase == $attrTableClass->getName()) || (!is_null($fk) && $fk == explode('fk_', $attrNameFromBase)[1]) == $attrTableClass->getName()) {
                        foreach ($this->classSetterMethodes as $methode) {
                            if (
                                (strtolower(substr($methode, 3)) == $attrNameFromBase)
                                ||
                                (!is_null($fk)
                                    &&
                                    ($fk == explode('fk_', $attrNameFromBase)[1] && $fk == strtolower(substr($methode, 3)))
                                )
                            ) {
                                if ($attrTableClass->getType() == 'DateTime') {
                                    $valueFieldFromBase = new \DateTime($valueFieldFromBase);
                                } elseif ($attrTableClass->getType() == 'int') {
                                    $valueFieldFromBase = (int) $valueFieldFromBase;
                                } elseif (!is_null($fk) && $fk == strtolower(substr($methode, 3))) {
                                    $repoTargetName = 'app\\Repository\\' . ucfirst($fk) . 'Repository';
                                    $repoTarget = new $repoTargetName();
                                    $valueFieldFromBase = $repoTarget->find($valueFieldFromBase, true);
                                } //elseif

                                $tableClass->$methode($valueFieldFromBase);
                                break;
                            } //if
                        } //foreach
                    } //if
                } //foreach

                /*
                 * Gestion de la relation ManyToMany
                 */
                if (!empty($fkManyToMany)) {
                    foreach ($fkManyToMany as $targetTableName => $targetId) {
                        if ($attrTableClass->getName() == $targetTableName) {
                            $targetRepositoryName = 'Src\\Repository\\' . ucfirst($targetTableName) . 'Repository';
                            $targetRepositoryClass = new $targetRepositoryName();
                            foreach($this->classSetterMethodes as $methode) {
                                foreach ($targetId as $objectId) {
                                    if (strtolower(substr($methode, 3)) == $targetTableName) {
                                        $tableClass->$methode($targetRepositoryClass->find($objectId, true));
                                    } //if
                                } //foreach
                            } //foreach
                        } //if
                    } //foreach
                } //if

                /**
                 * Gestion de la relation ManyToOne
                 */
                if (isset($manyToOneValues)) {
                    foreach ($manyToOneValues as $attrNameFromClass => $targetsObjects) {
                        foreach($this->classSetterMethodes as $methode) {
                            if (strtolower(substr($methode, 3)) == $attrNameFromClass && $attrNameFromClass == $attrTableClass->getName()) {
                                foreach($targetsObjects as $targetObject) {
                                    $tableClass->$methode($targetObject);
                                } //foreach
                            } //if
                        } //foreach
                    } //foreach
                } //if
            } //foreach

            return $tableClass;
        } //if
        return null;
    }

    public function getTable()
    {
        return new $this->table();
    }

    public function find(int $id = 0, bool $isManyToMany = false) : ?AbstractTable
    {
        $fields = $this->orm->buildRequest(
            'READ * WHERE id = ' . $id,
            strtolower($this->tableName)
        );
        return $this->build($fields[0], $isManyToMany);
    }

    public function findOneBy(array $criteres) : ?AbstractTable
    {
        $fields = $this->searchBy($criteres);

        if (empty($fields)) {
            return null;
        }

        return $this->build($fields[0]);
    }

    public function findBy(array $criteres, array $ignoreFkFields = []) : null|Array|AbstractTable
    {
        $fields = $this->searchBy($criteres);

        $tables = [];
        foreach ($fields as $field) {
            $tables[] = $this->build($field, false, $ignoreFkFields);
        }

        if (count($tables) > 1) return $tables;
        elseif(count($tables) == 1) return $tables[0];
        else return null;
    }

    public function findAll() : array
    {
        $fields = $this->orm->buildRequest('READ *', $this->tableName);

        $tables = [];
        foreach($fields as $field) {
            $tables[] = $this->build($field);
        }

        return $tables;
    }

    public function persist(AbstractTable $table)
    {
        $values = $this->callGetterMethodes($table);
        $isExist = null;
        if($table->getID() != 0) $isExist = $this->find($table->getID());
        if (!is_null($isExist)) {
            $req = 'UPDATE ';
            $req = $this->setValues($req, $values);
            $req .= ' WHERE id = ' . $table->getID();
        } else {
            $req = 'PERSIST ';
            $req = $this->setValues($req, $values);
        }
        $this->orm->buildRequest($req, $this->tableName);
    }

    public function delete(AbstractTable $table)
    {
        if (get_class($table) == $this->table) {
            $id = $table->getID();
            $req = 'DELETE WHERE id = ' . $id;

            $this->orm->buildRequest($req, $this->tableName);
        } else {
            throw new \ErrorException('La table à supprimer doit être de type "' . $this->table . '" ! Une instance de type "' . get_class($table) . '" a été donnée !');
        }
    }

    protected function makeRequest(String $request, String $tableName = null)
    {
        if (is_null($tableName)) $tableName = $this->tableName;

        return $this->orm->buildRequest(
            $request,
            $tableName,
        );
    }

    private function callGetterMethodes(AbstractTable $table) : array
    {
        $values = [];
        foreach($this->classGetterMethodes as $methode) {
            if ($methode != 'getID' && (!$table->$methode() instanceof Collection))
                $values += [substr($methode, 3) => $table->$methode()];
        }
        return $values;
    }

    private function setValues(String $req, array $values) : String
    {
        $i = 0;
        foreach ($values as $colmun => $value) {
            if (!$value instanceof Collection) {
                if ($value instanceof \DateTime) {
                    $value = $value->format('Y-n-j H:i:s');
                } elseif ($value instanceof AbstractTable) {
                    $value = $value->getID();
                    $colmun = 'fk_' . $colmun;
                }
                $req .= strtolower($colmun) . ' = "' . $value . '"';
                if ($i + 1 < sizeof($values)) {
                    $req .= ' AND ';
                }
            }
            $i++;
        }
        return $req;
    }

    private function searchBy(array $criteres) : Array|bool
    {
        $request = 'READ * WHERE ';
        $where = '';
        $i = 0;
        foreach ($criteres as $column => $values)
        {
            $where .= $column . ' = ' . $values;
            if($i + 1 < sizeof($criteres)) {
                $where .= ' AND ';
            }
            $i++;
        }

        $request .= $where;

        return $this->orm->buildRequest($request, $this->tableName);
    }
}