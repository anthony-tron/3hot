<?php

namespace modelo\CMD;

abstract class AbstractCMD
{
    protected $help = '';
    protected $cmdName;

    public function getHelp()
    {
        echo $this->cmdName . ' : ' . $this->help;
    }

    abstract public function run();
}