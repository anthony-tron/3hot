<?php

namespace modelo\CMD;

use modelo\ORM\ORM;

class CreateTableCMD extends AbstractCMD
{

    public function __construct()
    {
        $this->cmdName = 'create:table';
        $this->help = 'Commande permettant de générer une nouvelle table. Pour fonctionner, la la connexion à la base de donnée doit être configurée. Une classe Table et une table en BD sont générées';
    }

    public function run()
    {
        $tableName = readline("Le nom de votre table > ");

        $fields = [];
        if ($tableName != '') {
            echo 'Commencez à ajouter des champs ! ' . "\n";
            $addFields = true;
            while($addFields) {
                $fieldName = readline("Le nom de votre nouveau champ > ");
                $fieldName == false ? $addFields = false : $addFields = true;

                $typesFieldAvailable = [
                    'string' => 'permet de stocker du texte jusqu\'a 255 charactères',
                    'text' => 'Permet de stocker de longs texte',
                    'integer' => 'Permet de stocker des nombres entiers',
                    'real' => 'Permet de stocker des réel à virgule flottante',
                    'boolean' => 'Permet de stocker deux valeues "TRUE" ou "FALSE"',
                    'datetime' => 'Permet de stocker des dates au format DateTime',
                    'timestamp' => 'Permet de stocker des timestamp',
                    'relation' => 'Permet de créer une relation avec une autre table (OneToOne, ManyToOne, OneToMany, ManyToMany)'
                ];
                if ($addFields == true) {
                    $fieldName != false ? $fieldType = '?' : $fieldType = null ;
                    while($fieldType == '?') {
                        $fieldType = readline("Quel doit être le type de votre champ ? ([?] pour voir la liste) [string] > ");
                        switch ($fieldType) {
                            case '?':
                                foreach ($typesFieldAvailable as $type => $help) {
                                    echo '-- ' . $type . ' : ' . $help . "\n";
                                } //foreach
                                break;
                            case '':
                                $fieldType = 'string';
                                break;
                            default:
                                if (!array_key_exists($fieldType, $typesFieldAvailable)) {
                                    $fieldType = '?';
                                } //if
                        } //switch
                    } //while
                    $size = 0;
                    switch ($fieldType) {
                        case 'string':
                            while ($size <= 0 && is_int($size)) {
                                $size = readline("Combien de charactères peut t'il y avoir maximum (entre 1 et 255) [255] > ");
                                if ($size === '') $size = 255;
                                $size = (int) $size;
                            } //while
                            break;
                        case 'relation':
                            $targetTable = null;
                            while (is_null($targetTable)) {
                                $targetTable = readline("Avec quel table existante souhaitez vous créer une relation > ");
                                if (!class_exists('Src\Tables\\' . ucfirst($targetTable))) {
                                    echo 'La table ' . $targetTable . ' n\'a pas été trouvée !!';
                                    $targetTable = null;
                                } //if
                            } //while
                            $relationType = null;
                            $relationTypeAvailable = ['OneToOne', 'OneToMany', 'ManyToOne', 'ManyToMany'];
                            while (is_null($relationType)) {
                                echo "Les types de relations sont les suivants : \n " .
                                    "- 'OneToOne'   : $tableName peut avoir qu'un $targetTable et $targetTable peut avoir qu'un $tableName \n" .
                                    "- 'OneToMany'  : $tableName peut avoir qu'un $targetTable et $targetTable peut avoir plusieur $tableName \n" .
                                    "- 'ManyToOne'  : $tableName peut avoir plusieur $targetTable et $targetTable peut avoir qu'un $tableName \n" .
                                    "- 'ManyToMany' : $tableName peut avoir plusieur $targetTable et $targetTable peut avoir plusieur $tableName \n"
                                ;
                                $relationType = readline('Quel doit être le type de la relation > ');
                                if (!in_array($relationType, $relationTypeAvailable)) $relationType = null;
                            } //while
                            break;
                    } //switch

                    $isNull = null;
                    while (is_null($isNull)) {
                        $isNull = readline('Ce champ peut-il être null en base (y/n) [n] > ');
                        switch ($isNull) {
                            case 'y':
                            case 'Y':
                                $isNull = 'true';
                                break;

                            case '' :
                            case 'n':
                            case 'N':
                                $isNull = 'false';
                                break;

                            default:
                                $isNull = null;
                                break;
                        } //switch
                    } //while

                    $isUnique = null;
                    while (is_null($isUnique)) {
                        $isUnique = readline('Ce champ doit-il être unique en base (y/n) [n] > ');
                        switch ($isUnique) {
                            case 'y':
                            case 'Y':
                                $isUnique = 'true';
                                break;

                            case 'n':
                            case 'N':
                            case '' :
                                $isUnique = 'false';
                                break;

                            default:
                                $isUnique = null;
                                break;
                        } //switch
                    } //while
                    $fields[] = ['name' => $fieldName, 'type' => $fieldType, 'null' => $isNull, 'unique' => $isUnique, 'size' => $size ?? null, 'targetTable' => $targetTable ?? null, 'relationType' => $relationType ?? null];
                } //if
            } //while
            //On crée la Table et le Repo que si on a définit des champs
            if (!empty($fields)) {
                $orm = ORM::getORM();
                $req = 'NEW TABLE ' . strtolower($tableName) . ' FIELDS ( ';
                foreach ($fields as $field) {
                    $req .= 'NAME = ' . strtolower($field['name']) . ', TYPE = ' . strtolower($field['type']) . ', NULL = ' . $field['null'] . ', UNIQUE = ' . $field['unique'] ;
                    if (!is_null($field['size'])) {
                        $req .= ', SIZE = ' . $field['size'];
                    } if (!is_null($field['targetTable']) && !is_null($field['relationType'])) {
                        $req .= ', RELATION = ' . strtolower($field['relationType']) . ', FKTABLE = ' . strtolower($field['targetTable']);
                    }
                    $req .= '; ';
                } //foreach
                $req .= ')';
                $orm->buildRequest($req, strtolower($tableName));
            } //if
        } //if
    }
}