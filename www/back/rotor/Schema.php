<?php

namespace rotor;

abstract class Schema {
    public static function route($controller, string $action, string $actionMethod) {
        $reflection = new \ReflectionObject($controller);
        $success = false;

        foreach ($reflection->getMethods() as $method) {
            $methodName = $method->getName();
            $attribute =
                $method->getAttributes(Route::class)[0] ??
                $method->getAttributes(Get::class)[0] ??
                $method->getAttributes(Post::class)[0] ??
                $method->getAttributes(Put::class)[0] ??
                $method->getAttributes(Delete::class)[0] ??
                $method->getAttributes(Patch::class)[0] ??
                $method->getAttributes(Head::class)[0] ??
                null;
            $isExact = count ($method->getAttributes(Exact::class)) > 0;

            if (!empty($attribute)) {
                $route = $attribute->newInstance()->getRoute();

                // method check
                if (!in_array($actionMethod, $attribute->newInstance()->getMethods())) {
                    continue;
                }

                $matches = array();

                if ($isExact) {
                    if (
                        $route === $action
                        && self::compileMiddlewares($method)
                    ) {
                        $controller->$methodName();
                        $success = true;
                    }
                    continue; // skip regex check
                }

                if (
                    preg_match('/' . str_replace('/', '\/', $route) . '/', $action, $matches) === 1
                    && self::compileMiddlewares($method)
                ) {
                    if (count($matches) > 1) {
                        $controller->$methodName(array_slice($matches, 1));
                    } else {
                        $controller->$methodName();
                    }
                    $success = true;
                }
            }
        }

        if ($success)
            return;

        // fallbacks
        foreach ($reflection->getMethods() as $method) {
            $methodName = $method->getName();
            $attributes = $method->getAttributes(Fallback::class);

            if (count($attributes) > 0) {
                $controller->$methodName();
            }
        }
    }

    /**
     * return true if all middlewares return true
     * @param \ReflectionMethod $method
     * @return bool
     */
    private static function compileMiddlewares(\ReflectionMethod $method) : bool
    {
        $attributes = $method->getAttributes(Middleware::class);

        return \array_reduce(
            $attributes,
            fn ($carry, \ReflectionAttribute $attribute) =>
                $carry && $attribute->newInstance()->next(),
            true
        );
    }
}
