<?php


namespace rotor;


interface MiddlewareComposite
{

    /**
     * returns whether it's ok or not to go next step
     * @return bool
     */
    public function next(): bool;

}