<?php


namespace rotor;


#[\Attribute]
class Middleware
{
    public function __construct(
        private string $className
    ) {
    }

    public function next(): bool {
        $middleware = new $this->className();
        return $middleware->next();
    }
}