<?php


namespace app\middlewares;


use app\utils\Auth;
use rotor\MiddlewareComposite;

class AdminRequired implements MiddlewareComposite
{

    public function next(): bool
    {
        $user = Auth::getCurrentUser();

        return (
            !empty($user)
            && $user->getRole()->getName() === 'ADMIN'
        );
    }
}