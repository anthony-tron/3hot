<?php


namespace app\middlewares;


use app\utils\Auth;
use rotor\MiddlewareComposite;

class LoginRequired implements MiddlewareComposite
{

    public function next(): bool {
        return !empty(
            Auth::getCurrentUser()
        );
    }

}