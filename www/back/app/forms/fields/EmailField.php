<?php


namespace app\forms\fields;


class EmailField extends Field
{

    public function isValid(): bool
    {
        return filter_var(
            $this->getValue(),
            FILTER_VALIDATE_EMAIL
        ) !== false;
    }
}