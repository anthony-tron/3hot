<?php


namespace app\forms\fields;


use app\utils\Auth;

class UsernameField extends Field
{

    public function isValid(): bool
    {
        return \preg_match(
            '/^[a-zA-Z0-9\-_]{4,20}$/',
            $this->getValue()
        ) === 1;
    }
}