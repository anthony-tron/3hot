<?php


namespace app\forms;


interface Validatable
{
    public function isValid(): bool;
}