<?php


namespace app\forms;


use app\forms\fields\EmailField;
use app\forms\fields\PasswordField;
use app\forms\fields\UsernameField;

class EditUserForm extends Form
{

    public function __construct(
        string $username = null,
        string $email = null,
    )
    {
        parent::__construct([
            new UsernameField('username', $username),
            new EmailField('email', $email),
        ]);
    }

}