<?php


namespace app\forms;


use app\forms\fields\EmailField;
use app\forms\fields\PasswordField;
use app\forms\fields\UsernameField;

class CreateUserForm extends Form
{

    public function __construct(
        string $username = null,
        string $email = null,
        string $password = null,
        string $passwordConfirmation = null,
    )
    {
        parent::__construct([
            new UsernameField('username', $username),
            new EmailField('email', $email),
            new PasswordField('password', $password),
            new PasswordField('password-confirm', $passwordConfirmation),
        ]);
    }

}