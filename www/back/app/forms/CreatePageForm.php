<?php


namespace app\forms;


use app\forms\fields\TextField;

class CreatePageForm extends Form
{

    public function __construct(
        string $title,
        string $content,
        $sectionId,
    )
    {
        parent::__construct([
            new TextField('title', $title, 3, 128),
            new TextField('content', $content, 3, 8192),
            new TextField('section', $sectionId, 1, 3),
        ]);
    }

}