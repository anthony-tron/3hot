<?php


namespace app\forms;


abstract class Form implements Validatable
{

    public function __construct(
        private array $fields
    ) {
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return \array_reduce(
            $this->fields,
            fn ($carry, $field) => $carry && $field->isValid(),
            true
        );
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

}
