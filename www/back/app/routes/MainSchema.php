<?php

namespace app\routes;


use app\middlewares\LoginRequired;
use app\Repository\UserRepository;
use app\routes\admin\AdminSchema;
use app\routes\admin\AdminPageSchema;
use app\routes\admin\AdminSectionSchema;
use app\routes\admin\AdminUserSchema;
use app\utils\Auth;
use app\views\CompteView;
use app\views\IndexView;
use rotor\Exact;
use rotor\Get;
use rotor\Fallback;
use rotor\Middleware;
use rotor\Post;
use viewer\View;

class MainSchema {

    use AdminSchema;
    use AdminUserSchema;
    use AdminPageSchema;
    use AdminSectionSchema;
    use AuthSchema;
    use DocumentationSchema;

    #[Exact]
    #[Get('/')]
    function index() {
        if (empty(Auth::getCurrentUser())) {
            \header('Location: /login');
            return;
        }

        echo new IndexView();
    }

    #[Get('/test')]
    function test() {
        $roleRepo = new \app\Repository\RoleRepository();
        $role = $roleRepo->find(1);
        $role->setName("updated user");
        $roleRepo->persist($role);
    }


    #[Exact]
    #[Get('/compte')]
    #[Middleware(LoginRequired::class)]
    function compte() {
        try {
            echo new CompteView();
        } catch (\ValueError $e) {
            $this->fallback();
        }
    }

    #[Exact]
    #[Post('/compte/delete')]
    #[Middleware(LoginRequired::class)]
    function deleteCompte() {
        $user = Auth::getCurrentUser();
        Auth::logOut($user);

        $userRepo = new UserRepository();
        $userRepo->delete($user);

        \header('Location: /');
    }

    #[Fallback]
    function fallback() {
        http_response_code(404);
        echo View::fromFile('views/errors/404.php');
    }
}
