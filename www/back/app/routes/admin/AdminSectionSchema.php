<?php


namespace app\routes\admin;


use app\exceptions\ConstraintException;
use app\factories\DocumentationSectionFactory;
use app\forms\SectionForm;
use app\middlewares\AdminRequired;
use app\persistors\SectionPersistor;
use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\utils\Messages;
use app\views\AddSectionView;
use app\views\EditSectionView;
use rotor\Exact;
use rotor\Get;
use rotor\Middleware;
use rotor\Post;

trait AdminSectionSchema
{

    #[Exact]
    #[Get('/admin/section/add')]
    #[Middleware(AdminRequired::class)]
    function addSection() {

        echo new AddSectionView();
    }


    #[Exact]
    #[Post('/admin/section/add')]
    #[Middleware(AdminRequired::class)]
    function handlePostAddSection() {

        $form = new SectionForm(
            $_POST['name'],
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->addSection();
            return;
        }

        try {
            $section = (new DocumentationSectionFactory())->create(null, $_POST['name']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section non reconnue.');
            $this->addSection();
            return;
        }

        try {
            (new SectionPersistor())->persist($section);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre section possède déjà le même nom.');
            $this->addSection();
            return;
        }

        \header('Location: /admin');
    }


    #[Get('/admin/section/edit/(\d+)')]
    #[Middleware(AdminRequired::class)]
    function editSection($params) {

        [$id] = $params;

        $sectionRepository = new SectionRepository();
        $section = $sectionRepository->find($id);

        echo new EditSectionView($section);
    }


    #[Post('/admin/section/edit/(\d+)')]
    #[Middleware(AdminRequired::class)]
    function handlePostEditSection($params) {

        $form = new SectionForm(
            $_POST['name'],
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->editSection($params);
            return;
        }

        [$id] = $params;

        try {
            $section = (new DocumentationSectionFactory())->create($id, $_POST['name']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section non reconnue.');
            $this->editSection($params);
            return;
        }

        try {
            (new SectionPersistor())->persist($section);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre section possède déjà le même nom.');
            $this->editSection($params);
            return;
        }

        \header('Location: /admin');
    }


    #[Exact]
    #[Post('/admin/section/delete')]
    #[Middleware(AdminRequired::class)]
    public function deleteSection() {
        $ids = $_POST['ids'];

        $sectionRepository = new SectionRepository();

        foreach ($ids as $id) {
            $page = $sectionRepository->find($id);

            if (empty($page)) {
                Messages::$sharedInstance->add('error', 'Une erreur est survenue.');
                continue;
            }

            try {
                $sectionRepository->delete($page);
            } catch (\PDOException $e) {
                Messages::$sharedInstance->add('error', 'Impossible de supprimer une section contenant des pages.');
                continue;
            }
        }

        \header('Location: /admin');
    }

}