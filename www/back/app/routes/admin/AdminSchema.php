<?php

namespace app\routes\admin;


use app\middlewares\AdminRequired;
use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\Repository\UserRepository;
use app\Tables\User;
use app\utils\Auth;
use app\views\AdminDashboardView;
use rotor\Exact;
use rotor\Get;
use rotor\Middleware;

trait AdminSchema {


    #[Exact]
    #[Get('/admin')]
    #[Middleware(AdminRequired::class)]
    function adminIndex() {

        $userRepository = new UserRepository();
        $users = $userRepository->findAll();

        $pageRepository = new PageRepository();
        $pages = $pageRepository->findAll();

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        echo new AdminDashboardView($users, $sections, $pages);
    }

}
