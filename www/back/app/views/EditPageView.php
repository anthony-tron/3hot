<?php

namespace app\views;

use viewer\View;

class EditPageView extends LayoutView
{
    public function __construct($page, array $sections)
    {
        parent::__construct(
            'Modifier la page',
            View::fromFile('views/backoffice/EditPage.php', [
                'page' => $page,
                'sections' => $sections,
            ]),
        );
    }
}