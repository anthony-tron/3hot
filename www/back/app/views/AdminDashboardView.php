<?php


namespace app\views;


use app\utils\Messages;
use viewer\View;

class AdminDashboardView extends \viewer\View
{
    public function __construct(array $users, array $sections, array $pages, protected $params = array())
    {
        parent::__construct(
            'views/backoffice/Layout.php',
            \array_merge($params, [
                'h1' => 'Accueil',
                'error' => Messages::$sharedInstance->consume('error'),
                'success' => Messages::$sharedInstance->consume('success'),
                'body' => View::fromFile('views/backoffice/AdminPage.php', [
                    'users' => $users,
                    'pages' => $pages,
                    'sections' => $sections,
                ]),
            ])
        );
    }

}