<?php

namespace app\views;

use viewer\View;

class SignUpView extends LayoutView
{
    public function __construct($params = array())
    {
        parent::__construct(
            'Créer un compte',
            View::fromFile('views/Inscription.php', $params),
        );
    }
}