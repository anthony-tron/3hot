<?php


namespace app\views;


use viewer\View;

class EditSectionView extends LayoutView
{

    public function __construct($section) {
        parent::__construct(
            'Modifier une section',
            View::fromFile('views/backoffice/EditSection.php', [
                'section' => $section,
            ])
        );
    }

}