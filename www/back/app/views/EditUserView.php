<?php

namespace app\views;

use app\Tables\User;
use viewer\View;

class EditUserView extends LayoutView
{
    public function __construct(User $user, protected $params = array())
    {
        parent::__construct(
            'Modifier l\'utilisateur',
             View::fromFile('views/backoffice/EditUser.php', [
                'user' => $user
             ]),
            $params
        );
    }
}