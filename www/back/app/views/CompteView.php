<?php

namespace app\views;

use app\utils\Auth;
use viewer\View;

class CompteView extends LayoutView
{
    public function __construct($params = array())
    {
        parent::__construct(
            'Mon compte',
            View::fromFile('views/Compte.php', [
                'user' => Auth::getCurrentUser(),
            ]),
            $params
        );
    }
}