<?php

namespace app\views;

use viewer\View;

class IndexView extends LayoutView
{
    public function __construct()
    {
        parent::__construct(
            'Acceuil',
            View::fromFile('views/Welcome.php'),
            [
                'scripts' => [
                    'app.js',
                    'script.js',
                ]
            ]
        );
    }
}