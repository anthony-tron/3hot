<?php


namespace app\views\docs;


use viewer\View;

class IndexView extends View
{

    public function __construct($sections)
    {
        parent::__construct('views/Layout.php', [
            'h1' => 'Documentation',
            'body' => View::fromFile('views/docs/Index.php', [
                'sections' => $sections,
                'navigation' => new NavigationView($sections),
            ]),
        ]);
    }

}