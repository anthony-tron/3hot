<?php


namespace app\views\docs;


class NavigationView extends \viewer\View
{

    public function __construct(array $sections) {
        parent::__construct('views/docs/Navigation.php', [
            'sections' => $sections,
        ]);
    }

}