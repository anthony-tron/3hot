<?php


namespace app\views\docs;


use app\Tables\Page;
use app\Tables\Section;
use viewer\View;

class SectionDetailsView extends View
{

    public function __construct(Section $section, array $sections)
    {
        parent::__construct('views/Layout.php', [
            'h1' => 'Documentation / ' . $section->getName(),
            'body' => View::fromFile('views/docs/SectionDetails.php', [
                'section' => $section,
                'navigation' => new NavigationView($sections),
            ]),
        ]);
    }

}