<?php


namespace app\factories;


use app\exceptions\IllegalStateException;
use app\Repository\RoleRepository;
use app\Tables\User;

class EndUserFactory implements UserFactory
{

    /**
     * Creates a user with the role `USER`
     * Does not check for params validity
     * @param string $username
     * @param string $password
     * @param string $email
     * @return User
     */
    public function create(string $username, string $password, string $email): User
    {
        $roleRepository = new RoleRepository();
        $role = $roleRepository->findOneBy([
            'name' => 'USER',
        ]);

        if(empty($role)) {
            throw new IllegalStateException();
        }

        $hash = \password_hash($password, PASSWORD_BCRYPT);

        $user = new User();
        $user->setName($username);
        $user->setEmail($email);
        $user->setHash($hash);
        $user->setRole($role);

        return $user;
    }
}