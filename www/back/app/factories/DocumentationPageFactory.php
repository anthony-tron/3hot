<?php


namespace app\factories;


use app\exceptions\ConstraintException;
use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\Tables\Page;
use app\Tables\Section;

class DocumentationPageFactory implements PageFactory
{

    public function create(?int $id, string $title, string $content, int $sectionId)
    {
        $sectionRepository = new SectionRepository();
        $section = $sectionRepository->find($sectionId);

        if (empty($section)) {
            throw new ConstraintException();
        }

        if (empty($id)) {
            $page = new Page();
        } else {
            $pageRepository = new PageRepository();
            $page = $pageRepository->find($id);

            if (empty($page)) {
                throw new ConstraintException();
            }
        }

        $page->setTitle($title);
        $page->setContent($content);
        $page->setSection($section);

        return $page;
    }
}