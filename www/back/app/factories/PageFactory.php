<?php


namespace app\factories;


use app\Tables\Section;

interface PageFactory
{

    public function create(?int $id, string $title, string $content, int $sectionId);

}