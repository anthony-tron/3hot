<?php


namespace app\factories;


use app\exceptions\ConstraintException;
use app\Repository\SectionRepository;
use app\Tables\Section;

class DocumentationSectionFactory implements SectionFactory
{

    public function create(?int $id, string $name): Section
    {
        $sectionRepository = new SectionRepository();

        if (empty($id)) {
            $section = new Section();
        } else {
            $section = $sectionRepository->find($id);

            if (empty($section)) {
                throw new ConstraintException();
            }
        }

        $section->setName($name);

        return $section;
    }
}