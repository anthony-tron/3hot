<?php


namespace app\persistors;


use app\exceptions\ConstraintException;
use app\Repository\SectionRepository;
use app\Tables\Section;

/**
 * Class SectionPersistor
 * @implements Persistor<Section>
 * @package app\persistors
 */
class SectionPersistor implements Persistor
{

    /**
     * @inheritDoc
     * @param Section $section
     */
    public function persist($section): void
    {
        $sectionRepository = new SectionRepository();

        $sectionWithSameName = $sectionRepository->findOneBy([
            'name' => $section->getName(),
        ]);

        if (!empty($sectionWithSameName)) {
            throw new ConstraintException();
        }

        $sectionRepository->persist($section);
    }
}