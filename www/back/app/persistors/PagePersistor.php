<?php


namespace app\persistors;


use app\exceptions\ConstraintException;
use app\Repository\PageRepository;
use app\Repository\UserRepository;
use app\Tables\Page;
use app\Tables\User;

/**
 * Class PagePersistor
 * @implements Persistor<Page>
 * @package app\persistors
 */
class PagePersistor implements Persistor
{

    /**
     * @inheritDoc
     * @param Page $page
     */
    public function persist($page): void
    {
        $pageRepository = new PageRepository();

        $pageWithSameTitle = $pageRepository->findOneBy([
            'title' => $page->getTitle(),
        ]);

        if (
               !empty($pageWithSameTitle)
            && $pageWithSameTitle->getID() !== $page->getID()
        ) {
            throw new ConstraintException();
        }

        $pageRepository->persist($page);
    }
}