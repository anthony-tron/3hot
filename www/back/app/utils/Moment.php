<?php

namespace app\utils;

class Moment
{
    public static function formatTimeStamp(\DateTime $date) : String
    {
        return $date->format("Y-m-d H:i:s");
    }
}