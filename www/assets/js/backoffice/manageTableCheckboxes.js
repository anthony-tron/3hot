//Au clic sur la checkbox globale pages
document.querySelector("#selectAllPages").addEventListener("click", () => {
    updateCheckboxesState("#selectAllPages",".tbody-pages");
    updateButtonState(".btn-delete-selection-pages",".tbody-pages");
});

const checkboxesPages = document.querySelectorAll(".tbody-pages input[type=checkbox]"); //Récupère toutes les checkboxes
checkboxesPages.forEach(checkbox => {
    eventOnCheckbox(checkbox,"#selectAllPages",".tbody-pages",".btn-delete-selection-pages")
})

//Au clic sur la checkbox globale sections
document.querySelector("#selectAllSections").addEventListener("click", () => {
    updateCheckboxesState("#selectAllSections",".tbody-sections");
    updateButtonState(".btn-delete-selection-sections",".tbody-sections");
});

const checkboxesSections = document.querySelectorAll(".tbody-sections input[type=checkbox]"); //Récupère toutes les checkboxes
checkboxesSections.forEach(checkbox => {
    eventOnCheckbox(checkbox,"#selectAllSections",".tbody-sections",".btn-delete-selection-sections")
})

//Au clic sur la checkbox globale users
document.querySelector("#selectAllUsers").addEventListener("click", () => {
    updateCheckboxesState("#selectAllUsers",".tbody-users");
    updateButtonState(".btn-delete-selection-users",".tbody-users");
});

const checkboxesUsers = document.querySelectorAll(".tbody-users input[type=checkbox]"); //Récupère toutes les checkboxes
checkboxesUsers.forEach(checkbox => {
    eventOnCheckbox(checkbox,"#selectAllUsers",".tbody-users",".btn-delete-selection-users")
})

//Fonctions

function updateCheckboxesState(identifier,classTarget){
    let checkboxes = document.querySelectorAll(classTarget + " input[type=checkbox]");
    let $this = document.querySelector(identifier);

    //On attribue à toutes les checkboxes l'état de la checkbox globale
    checkboxes.forEach(checkbox => {
        checkbox.checked = $this.checked;
    })
}

function updateButtonState(identifier,classTarget){
    //On désactive le bouton si aucune checkbox n'est cochée
    const atLeastOneCheck = document.querySelectorAll(classTarget + " input:checked").length !== 0;//True : au moins une checkbox cochée, False : aucune checkbox cochée
    document.querySelector(identifier).disabled = !atLeastOneCheck;
}

function eventOnCheckbox(checkbox,identifier,classTarget,button){
    //Au click sur la checkbox
    checkbox.addEventListener("click", () => {
        //Si on la décoche, on décoche la checkbox globale
        if (!checkbox.checked) {
            document.querySelector(identifier).checked = false;
        }

        //On désactive le bouton pour supprimer si aucune checkbox n'est cochée
        updateButtonState(button,classTarget);
    })
}