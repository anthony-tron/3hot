export const makeChartConfig = (data1, data2) => {
    return {
        type: 'line',
        data: {
            labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
            datasets: [
                {
                    label: 'Température intérieure',
                    data: data1,
                    backgroundColor: [
                        'rgba(255,174,0,0.2)',
                    ],
                    borderColor: [
                        '#ffae00',
                    ],
                    borderWidth: 3,
                    tension: 0.2
                },
                {
                    label: 'Température extérieure',
                    data: data2,
                    backgroundColor: [
                        'rgba(164,164,164,0.2)',
                    ],
                    borderColor: [
                        'rgb(164,164,164)',
                    ],
                    borderWidth: 3,
                    tension: 0.2
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
        }
    };
};
