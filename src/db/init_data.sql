INSERT INTO `3hot_core`.role (id, name) VALUES (1, 'USER');
INSERT INTO `3hot_core`.role (id, name) VALUES (2, 'ADMIN');


-- docs' sections
INSERT INTO `3hot_core`.section (id, name) VALUES (4, 'Manuel utilisateur');
INSERT INTO `3hot_core`.section (id, name) VALUES (9, 'Documentation technique');


-- docs' pages

INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (16, 9, 'Configuration des ports et de la base de données', 'L’essentiel de la configuration se fait dans le fichier .env.

Il faut relancer le service entre chaque modification de ce fichier pour que les valeurs données par le fichier .env soient actualisées.

Les ports rendus publics :

EXPOSED_HTTP_PORT : le port HTTP
EXPOSED_HTTPS_PORT : le port HTTPS
EXPOSED_DB_PORT : le port mysql

La configuration de la base de données :

DB_PORT : le port utilise par mysql dans le réseau interne.
DB_TYPE : type du SGBD
DB_HOST : nom de l’hôte sur le réseau interne qui héberge la BD
DB_NAME : nom de la base de donnees
DB_USER : identifiant de l’utilisateur avec qui executer les requetes
DB_PASSWORD : mot de passe en clair de l’utilisateur
DB_STATE : si « true », active la BD.
');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (17, 9, 'Configuration du mailing', 'Copier le fichier `./etc/msmtprc.template` dans `./etc/msmtprc`.

Remplacer `MAIL_HOST`, `MAIL_PORT`, `MAIL_USER`, et `MAIL_PASSWORD`.

Plus d’informations : https://doc.ubuntu-fr.org/msmtp#configuration

POP3 et IMAP doivent être activés chez l’hôte renseigné dans MAIL_HOST.
');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (18, 9, 'Déploiement', 'Configuration minimale :
- la commande docker-compose
- la commande npm
- mailing configuré (voir la page sur la configuration du mailing)
- fichier .env déjà configuré (voir page sur la configuration de la base de données)

Exécuter `npm run build` pour obtenir les fichiers CSS.

Exécuter `docker-compose build && docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`.

La base de données peut prendre plusieurs minutes pour s’initialiser.
Pour consulter les logs :
- `docker ps`
- `docker logs -f <ID_CONTENEUR>`

Générer les classes de l’ORM :
- se connecter a web_server : `docker exec -ti <ID_CONTENEUR>`
- exécuter `modelo orm:database-import`.

L’application web doit maintenant être accessible.

Recommandé : lire la page « Créer un compte administrateur ».
');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (19, 9, 'Modifier le modèle de données', 'Afin de mettre à jour le modèle de données :

- modifier le script `src/db/init.sql`
- afficher la liste des conteneurs : `docker ps`
- stopper le conteneur de mysql : `docker stop <ID_CONTENEUR>`
- afficher la liste des volumes : `docker volume ls`
- supprimer le volume de mysql : `docker volume rm <ID_VOLUME>`
- relancer le service mysql
- se connecter au service `web_server` : `docker exec -ti … bash`
- executer `modelo orm:database-import`

Les migrations ne sont pas encore supportées de manière automatique.
');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (20, 9, 'Créer un compte administrateur', 'Pré-requis :
- Lire la page `Déploiement`

S’inscrire :
- Aller sur l’accueil du site, cliquer sur « Inscrivez-vous » ou accéder directement à l’url `votresite.com/signup`
- Remplir et pousser le formulaire, retenir le nom d’utilisateur
- Vérifier que le compte est bien créé en essayant de se connecter via l’interface

Modifier le rôle :
- Établir une connexion à la base de données (les informations de connexion sont dans le fichier .env)
- Trouver le tuple correspondant à votre utilisateur dans la table `users`
- Mettre à jour le champ `fk_role` à 2.

Vérifier :
- Pas besoin de se deconnecter
- Essayer d’accéder a `votresite.com/admin`
- Si une URL 404 s’affiche, verifier que vous êtes connectés avec le bon utilisateur et que son rôle est celui d’un administrateur.
');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (21, 4, 'Créer un compte', 'Pour créer un compte, l''utilisateur doit se rendre sur `votresite.com/signup` puis remplir tous les champs du formulaire :

- nom d''utilisateur : suite de chiffres et de caractères sans espace

- adresse mail : doit être valide

- mot de passe : 8 caractères minimum, 1 majuscule minimum et 1 chiffre minimum');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (22, 4, 'Se connecter', 'Pour se connecter, cliquer sur le lien `Connexion` ou se rendre à l''adresse votresite.com/login, puis saisir le nom d''utilisateur, le mot de passe, et valider.
Il est possible de récupérer un mot de passe perdu en cliquant sur `Mot de passe oublié ?`. Les instructions sont envoyées par mail.');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (23, 4, 'Accueil', 'L''utilisateur peut consulter les données de deux capteurs sur la page d''accueil. Il s''agit de la température intérieure et extérieure.');
INSERT INTO `3hot_core`.page (id, fk_section, title, content) VALUES (24, 4, 'Documentation', 'L''utilisateur peut consulter des pages de documentation expliquant le fonctionnement du site et du framework.');
